## 主要功能
- 本项目用于生成 JPA 项目的model，dao 代码，目前支持 MSSQLSERVER,MySQL和postgreSQL数据库
- 接下来将实现 service 和 controller 代码生成
- 实现多个关联实体拼接成一个内容丰富的 MAP 对象

## 注意：
- 不要修改sys-config.properties文件
- 需要修改custom-config.properties属性值以适应自己的项目，允许修改和增加属性，但不要删除
- 模板可自定义，也可修改resources/template中的模板，增加的模板变量请在custom-config.properties定义
- 本项目用于公司内部项目开发，数据库设计规范和代码命名规范需遵循公司内部约定
 
## 技术方案：
  spring + freemarker

## 其他
- 本项目由 [generate-maven-project-service](https://git.oschina.net/dynastqin/generate-maven-project-service.git) 增加修改完成，感谢 @阿狸包子店 

