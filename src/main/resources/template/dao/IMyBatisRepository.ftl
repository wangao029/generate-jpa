package ${ftl_dao_package};

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* 标记接口。
* 使用说明：所有DAO 定义成接口并继承IMyBatisRepository，由Spring Context扫描，动态生成代理的实现类
*
* @author ${ftl_author}
* @date ${ftl_now}
* @version ${ftl_version}
*/
public interface IMyBatisRepository<T> {
    int insert(T t);
    int updateById(T t);// 根据主键来更新
    int deleteById(int key);// 根据主键来删除
    T getById(int key);
    List<T> getListByCriteria(@Param("queryObject") T queryObjects);

}
