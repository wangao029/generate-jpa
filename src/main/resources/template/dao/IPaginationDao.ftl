package ${ftl_dao_package};

import ${ftl_model_package}.pagination.PageAttribute;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
* 分页接口
* 定义了DAO分页功能的规范
* 使用说明：DAO如果有分页功能，则继承该接口
*
* @author ${ftl_author}
* @date ${ftl_now}
* @version ${ftl_version}
*/
public interface IPaginationDao<T> {

    /**
    * 根据过滤条件查询出数据列表（用于分页）
    * @param page 分页参数对象
    * @param queryObject 过滤条件对象
    * @param otherParam 其它过滤条件
    */
    List<T> queryPageList(@Param("page") PageAttribute page, @Param("queryObject") T queryObject, @Param("otherParam") Map<String, Object> otherParam);

    /**
    * 根据过滤条件统计记录条数（用于分页）
    * @param queryObject 过滤条件对象
    * @param otherParam 其它过滤条件
    */
    int count(@Param("queryObject") T queryObject, @Param("otherParam") Map<String, Object> otherParam);

}
