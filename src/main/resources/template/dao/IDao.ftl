package ${ftl_dao_package};

import ${ftl_model_package}.${className};

/**
* dao
*
* @author ${ftl_author}
* @date ${ftl_now}
* @version ${ftl_version}
*/
public interface I${className}Dao extends IMyBatisRepository<${className}>,IPaginationDao<${className}> {
}
