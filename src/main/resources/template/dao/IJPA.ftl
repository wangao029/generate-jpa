package ${ftl_dao_package};

import ${ftl_model_package}.${className};
import ${ftl_dao_package}.common.CrudDao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

/**
* ${className}Dao
*
* @author ${ftl_author}
* @date ${ftl_now}
* @version ${ftl_version}
*/
public interface ${className}Dao extends CrudDao<${className},Long> {

    /**
    * 根据编号查询单个对象
    *
    * @param no 编号（逻辑主键）
    */
    @Query("select o from ${className} o where ${logicId} = ?1 and fisDeleted = 0")
    public ${className} findOne(String no);

    /**
    * 根据编号逻辑删除
    *
    * @param no 编号（逻辑主键）
    */
    @Transactional
    @Modifying
    @Query("update ${className} set fisDeleted = 1 where ${logicId} = ?1 ")
    public void delete(String no);

    /**
    * 根据编号列表查询对象列表
    *
    * @param nos 编号列表（逻辑主键）
    */
    @Query("select o from ${className} o where ${logicId} in (?1) and fisDeleted = 0")
    public List<${className}> find(List<String> nos);
}
