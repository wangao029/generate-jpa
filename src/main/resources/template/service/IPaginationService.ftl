package ${ftl_service_package};

import ${ftl_model_package}.pagination.PageAttribute;
import ${ftl_model_package}.pagination.PageList;

import java.util.Map;

/**
* 分页业务公共接口
*
* @author ${ftl_author}
* @date ${ftl_now}
* @version ${ftl_version}
*/
public interface IPaginationService<T> {

    /**
    * 根据过滤条件查询出数据列表（用于分页）
    * @param page 分页参数对象
    * @param queryObject 过滤条件对象
    * @param otherParam 其它过滤条件
    */
    PageList<T> queryEntityPageList(PageAttribute page, T queryObject, Map<String, Object> otherParam);
}
