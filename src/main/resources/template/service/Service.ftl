package ${ftl_service_package}.impl;

import ${ftl_dao_package}.I${className}Dao;
import ${ftl_dao_package}.IMyBatisRepository;
import ${ftl_model_package}.${className};
import ${ftl_model_package}.pagination.PageAttribute;
import ${ftl_model_package}.pagination.PageList;
import ${ftl_model_package}.pagination.PaginationTemplate;
import ${ftl_service_package}.AbstractBaseService;
import ${ftl_service_package}.I${className}Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
* service impl
*
* @author ${ftl_author}
* @date ${ftl_now}
* @version ${ftl_version}
*/
@Service
public class ${className}Service extends AbstractBaseService<${className}> implements I${className}Service {
    @Resource
    private I${className}Dao ${className?uncap_first}Dao;

    @Override
    protected IMyBatisRepository<${className}> getMyBatisRepository() {
        return ${className?uncap_first}Dao;
    }

    @Override
    public PageList<${className}> queryEntityPageList(PageAttribute page, ${className} queryObject, Map<String, Object> otherParam) {
        return new PaginationTemplate<${className}>(${className?uncap_first}Dao)
            .queryPageList(page, queryObject, otherParam);
    }

}

