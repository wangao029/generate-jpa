package ${ftl_service_package};

import com.google.common.base.Preconditions;
import ${ftl_dao_package}.IMyBatisRepository;
import ${ftl_package}.exception.${ftl_exception_class};

import java.util.List;

/**
* 通用的业务接口的实现
*
* @author ${ftl_author}
* @date ${ftl_now}
* @version ${ftl_version}
*/
public abstract class AbstractBaseService<T> implements IBaseService<T> {


    /** 子类需要注入特定的DAO实现 */
    protected abstract IMyBatisRepository<T> getMyBatisRepository();

    /**
    * 注意：只能做一些对于参数的预操作，不能涉及事务。涉及事务请覆盖create
    * 子类可根据情况覆盖此方法
    * 用于在执行新增操作之前进行预处理
    * @param entity 需新增的实体
    */
    protected void preCreate(T entity) throws ${ftl_exception_class} {
        return;
    }

    @Override
    public void create(T entity) throws ${ftl_exception_class} {
        Preconditions.checkNotNull(entity, "新增对象不能为null");
         preCreate(entity);
        int successCount = getMyBatisRepository().insert(entity);
        if (1 != successCount) {
        throw new ${ftl_exception_class}("新增对象-数据库失败");
    }
    }
     /**
    * 注意：只能做一些对于参数的预操作，不能涉及事务。涉及事务请覆盖modifyEntityById
    * 子类可根据情况覆盖此方法
    * 用于在执行修改操作之前进行预处理
    * @param entity 需修改的实体
    */
    protected void preModify(T entity) throws ${ftl_exception_class} {
    return;
    }

    @Override
    public void modifyEntityById(T entity) throws ${ftl_exception_class} {
        Preconditions.checkNotNull(entity, "修改对象不能为null");
         preModify(entity);
        int successCount = getMyBatisRepository().updateById(entity);
        if (1 != successCount) {
        throw new ${ftl_exception_class}("修改对象-数据库失败");
        }
    }
     /**
    * 注意：只能做一些对于参数的预操作，不能涉及事务。涉及事务请覆盖deleteEntityById
    * 子类可根据情况覆盖此方法
    * 用于在执行删除操作之前进行预处理
    * @param id 需删除实体的ID
    */
    protected void preDelete(Integer id) throws ${ftl_exception_class} {
        return;
    }

    @Override
    public void deleteEntityById(Integer id) throws ${ftl_exception_class} {
        Preconditions.checkNotNull(id, "删除对象id不能为null");
         preDelete(id);
        int successCount = getMyBatisRepository().deleteById(id);
        if (1 != successCount) {
        throw new ${ftl_exception_class}("删除对象-数据库失败");
        }
    }

    /**
    * 注意：只能做一些对于参数的预操作，不能涉及事务。涉及事务请覆盖getEntityById
    * 子类可根据情况覆盖此方法
    * 用于在执行查询操作之前进行预处理
    * @param id 需查询实体的ID
    */
    protected void preGet(Integer id) throws ${ftl_exception_class} {
        return;
    }
    @Override
    public T getEntityById(Integer id) throws ${ftl_exception_class} {
        Preconditions.checkNotNull(id, "查询对象id不能为null");
         preGet(id);
        return getMyBatisRepository().getById(id);
    }
     /**
    * 注意：只能做一些对于参数的预操作，不能涉及事务。涉及事务请覆盖queryEntityList
    * 子类可根据情况覆盖此方法
    * 用于在执行查询列表操作之前进行预处理
    * @param queryObject 查询参数对象
    */
    protected void preQuery(T queryObject) throws ${ftl_exception_class} {
        return;
    }

    @Override
    public List<T> queryEntityList(T queryObject) throws ${ftl_exception_class} {
        Preconditions.checkNotNull(queryObject, "查询参数对象不能为null");
        preQuery(queryObject);
        return getMyBatisRepository().getListByCriteria(queryObject);
    }
}