package ${ftl_model_package};

import java.io.Serializable;
import javax.persistence.*;
import io.zzit.wltrip.model.common.CommonModel;

/**
* model ${className}
*
* @author ${ftl_author}
* @date ${ftl_now}
* @version ${ftl_version}
*/

@Entity
@Table(name = "${tableName}", schema = "dbo", catalog = "wlproxy")
public class ${className} implements Serializable,CommonModel{
<#list classFields as v>
    private ${v.type} ${v.field} <#if v.field=="fIsDeleted">= false</#if>;
</#list>

<#list classFields as v>

    <#if v.isPrimaryKey>
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    <#else>
    @Column(name = "${v.columnName}"<#if !v.isNullable>, nullable = false</#if><#if v.columnName=="FCreatedTime"||v.columnName=="FCreatorID"||v.columnName=="FCreatorName">, updatable = false </#if>)
    </#if>
    public ${v.type} get${v.field?cap_first}(){
        return this.${v.field};
    }

    public void set${v.field?cap_first}(${v.type} ${v.field}){
        this.${v.field} = ${v.field};
    }
</#list>
}