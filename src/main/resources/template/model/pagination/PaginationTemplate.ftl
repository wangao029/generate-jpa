package ${ftl_model_package}.pagination;

import ${ftl_dao_package}.IPaginationDao;

import java.util.List;
import java.util.Map;

/**
* 分页模板，用于Service进行分页查询
*
* @author ${ftl_author}
* @date ${ftl_now}
* @version ${ftl_version}
*/
public class PaginationTemplate<T> {

    /** MyBatisDao */
    private IPaginationDao<T> paginationDao;

    /**
    * 封闭无参构造器，必须持有Dao
    */
    public PaginationTemplate(IPaginationDao<T> paginationDao) {
        this.paginationDao = paginationDao;
    }

    /**
    * 分页查询数据方法
    * @param pageAttr 分页参数对象
    * @param queryObject 过滤条件对象
    * @param otherParam 其它过滤条件
    */
    public PageList<T> queryPageList(PageAttribute pageAttr, T queryObject, Map<String, Object> otherParam) {

        if(null == pageAttr)
           pageAttr = PageAttribute.getInstance();

        //count记录数
        int count = paginationDao.count(queryObject, otherParam);
        if (0 == count)
            return PageList.<T>getEmptyInstance();
         List<T> datas = paginationDao.queryPageList(pageAttr, queryObject, otherParam);
          return PageList.getInstance(datas, Page.getInstance(pageAttr, count));
      }

      /**
      * 分页查询数据方法，除主题查询参数对象外，不需要其它额外参数
      * @param pageAttr 分页参数对象
      * @param queryObject 过滤条件对象
      */
      public PageList<T> queryPageList(PageAttribute pageAttr, T queryObject) {
          return queryPageList(pageAttr, queryObject, null);
      }
}
